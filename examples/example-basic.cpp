#include <messeplatz/Application.hpp>
#include <messeplatz/StatefulWidget.hpp>

#include <QPushButton>
#include <QVBoxLayout>

#include <iostream>

class HomeScreen : public msp::StatefulWidget {
public:
  explicit HomeScreen(StatefulWidget *parent) : msp::StatefulWidget(parent) {}
  msp::WidgetStateBase *createState() override;
};

class HomeScreenState : public msp::State<HomeScreen> {
  QString text = QStringLiteral("Change text");

public:
  explicit HomeScreenState(HomeScreen *widget) : msp::State<HomeScreen>(widget) {}
  msp::Widget *build() override {
    auto *content = new msp::Widget();
    auto *column = new QVBoxLayout(content);
    column->setContentsMargins(0, 0, 0, 0);
    column->setSpacing(0);
    auto *button_ChangeText = new QPushButton(text, content);
    column->addWidget(button_ChangeText);

    connect(button_ChangeText, &QPushButton::clicked, this, [this] {
      setState([this] { text = QStringLiteral("Text changed"); });
    });

    auto *button_Exit = new QPushButton(QStringLiteral("Exit"), content);
    column->addWidget(button_Exit);

    connect(button_Exit, &QPushButton::clicked, this, [this] {
      setState([this] { widget->close(); });
    });
    return content;
  }
};

msp::WidgetStateBase *HomeScreen::createState() { return new HomeScreenState(this); }

int main(int argc, char *argv[]) {
  msp::Application app(argc, argv);
  app.withHome(new HomeScreen(nullptr));
  return msp::runApp(&app);
}
