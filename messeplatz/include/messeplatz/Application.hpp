#ifndef MESSEPLATZ_APPLICATION_HPP
#define MESSEPLATZ_APPLICATION_HPP
#include <QApplication>

#include "Widget.hpp"
namespace msp {
class Application : public QApplication {
public:
  Application(int &argc, char **argv);

  Application &withHome(Widget *home);

  ~Application();
  Widget *Home();

private:
  Widget *m_Home{};
};

int runApp(Application *app);
} // namespace msp
#endif // MESSEPLATZ_APPLICATION_HPP
