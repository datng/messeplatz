#ifndef MESSEPLATZ_EXPANSIONTILE_HPP
#define MESSEPLATZ_EXPANSIONTILE_HPP
#include "StatefulWidget.hpp"

#include "ListTileControlAffinity.hpp"

class QToolButton;

namespace msp {
class ExpansionTile : public StatefulWidget {
  friend class ExpansionTileState;

public:
  explicit ExpansionTile(Widget *parent = nullptr);
  virtual ~ExpansionTile();

  WidgetStateBase *createState() override;

  /// The widgets that are displayed when the tile expands.
  ///
  /// Typically [ListTile] widgets.
  template <class C = ExpansionTile, class = IsBased<ExpansionTile, C>>
  C *children(std::vector<Widget *> eChildren) {
    mspCheckCast<C>();

    m_Children = eChildren;
    return this;
  }

  /// Typically used to force the expansion arrow icon to the tile's leading or
  /// trailing edge.
  ///
  /// By default, the value of `controlAffinity` is
  /// [ListTileControlAffinity.platform], which means that the expansion arrow
  /// icon will appear on the tile's trailing edge.
  template <class C = ExpansionTile, class = IsBased<ExpansionTile, C>>
  C *controlAffinity(ListTileControlAffinity eControlAfinity) {
    mspCheckCast<C>();

    m_ControlAfinity = eControlAfinity;
    return this;
  }

  /// A widget to display before the title.
  ///
  /// Typically a [CircleAvatar] widget.
  ///
  /// Note that depending on the value of [controlAffinity], the [leading]
  /// widget may replace the rotating expansion arrow icon.
  template <class C = ExpansionTile, class = IsBased<ExpansionTile, C>>
  C *leading(Widget *eLeading) {
    mspCheckCast<C>();

    m_Leading = eLeading;
    return this;
  }

  /// A widget to display after the title.
  ///
  /// Note that depending on the value of [controlAffinity], the [trailing]
  /// widget may replace the rotating expansion arrow icon.
  template <class C = ExpansionTile, class = IsBased<ExpansionTile, C>>
  C *trailing(Widget *eTrailing) {
    mspCheckCast<C>();

    m_Trailing = eTrailing;
    return this;
  }

  void mspReparentChildren() override;

protected:
  std::vector<Widget *> m_Children;
  ListTileControlAffinity m_ControlAfinity;
  Widget *m_Leading = nullptr;
  Widget *m_Title = nullptr;
  Widget *m_Trailing = nullptr;
  bool m_IsExpanded = false;
};

class ExpansionTileState : public State<ExpansionTile> {
public:
  ExpansionTileState(ExpansionTile *parent);
  Widget *build() override;

private:
  QToolButton *buildArrowButton(Widget *parent);
};
} // namespace msp
#endif // MESSEPLATZ_EXPANSIONTILE_HPP
