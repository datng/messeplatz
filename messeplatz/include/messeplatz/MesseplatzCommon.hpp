#ifndef MESSEPLATZ_MESSEPLATZCOMMON_HPP
#define MESSEPLATZ_MESSEPLATZCOMMON_HPP
#include <type_traits>
namespace msp {
template <typename Base, typename ToBeChecked>
using IsBased = typename std::enable_if_t<std::is_base_of_v<Base, ToBeChecked>>;
} // namespace msp
#endif // MESSEPLATZ_MESSEPLATZCOMMON_HPP
