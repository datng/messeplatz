#ifndef MESSEPLATZ_STATE_HPP
#define MESSEPLATZ_STATE_HPP
#include "WidgetStateBase.hpp"

#include "MesseplatzCommon.hpp"

#include <QVBoxLayout>

#include <functional>

namespace msp {
class StatefulWidget;

template < //
    class _mspWidget,
    class = IsBased<StatefulWidget, _mspWidget>>
class State : public WidgetStateBase {
public:
  State(_mspWidget *widget) : widget(widget) {
    assert(widget != nullptr);
    setParent(widget);
  }
  void initState() override { setState(); }

  void setState(std::function<void()> fn = []() {}) override {
    fn();
    widget->mspReparentChildren();
    delete widget->layout();
    auto newLayout = new QVBoxLayout(widget);
    newLayout->setContentsMargins(0, 0, 0, 0);
    auto content = build();
    content->setParent(widget);
    newLayout->addWidget(content);
    connect(newLayout, &QObject::destroyed, content, &QObject::deleteLater);
    widget->setLayout(newLayout);
  }

protected:
  _mspWidget *widget;
};
} // namespace msp
#endif // MESSEPLATZ_STATE_HPP
