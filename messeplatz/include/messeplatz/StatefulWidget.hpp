#ifndef MESSEPLATZ_STATEFULWIDGET_HPP
#define MESSEPLATZ_STATEFULWIDGET_HPP
#include "Widget.hpp"

#include "State.hpp"

#include <QVBoxLayout>

namespace msp {
class StatefulWidget : public Widget {
public:
  explicit StatefulWidget(Widget *parent = nullptr);
  virtual ~StatefulWidget();

  /**
   * @details While building widgets, subwidgets could be added into layouts,
   * which reparents them. If left unchecked, they will be deleted in the next
   * build, which will result in a segfault.
   *
   * Override this function to reparent such widgets (until a more robust
   * solution is found).
   */
  virtual void mspReparentChildren() {}

protected:
  virtual WidgetStateBase *createState() = 0;

private:
  std::unique_ptr<WidgetStateBase> m_State{};
};

} // namespace msp
#endif // MESSEPLATZ_STATEFULWIDGET_HPP
