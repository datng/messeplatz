#ifndef MESSEPLATZ_STATELESSWIDGET_HPP
#define MESSEPLATZ_STATELESSWIDGET_HPP
#include "Widget.hpp"

namespace msp {
class StatelessWidget : public Widget {
public:
  StatelessWidget(Widget *parent = nullptr);
  virtual ~StatelessWidget();

  virtual Widget *build() = 0;
};
} // namespace msp
#endif // MESSEPLATZ_STATELESSWIDGET_HPP
