#ifndef MESSEPLATZ_TEXT_HPP
#define MESSEPLATZ_TEXT_HPP
#include "StatelessWidget.hpp"

namespace msp {
class Text : public StatelessWidget {
public:
  Text(std::string data, Widget *parent = nullptr);
  virtual ~Text();
  Widget *build() override;

protected:
  std::string m_Data;
};
} // namespace msp
#endif // MESSEPLATZ_TEXT_HPP
