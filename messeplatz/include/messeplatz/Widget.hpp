#ifndef MESSEPLATZ_WIDGET_HPP
#define MESSEPLATZ_WIDGET_HPP
#include <QWidget>

namespace msp {
class Widget : public QWidget {
public:
  Widget(Widget *parent = nullptr);
  virtual ~Widget();

protected:
  /**
   * @brief mspCheckCast is used to help with chaining setters in subclasses.
   *
   * @details In Flutter, when a widget is created, besides the mandatory
   * paremeter, other parameters can be given to the constructor.
   * Take, for example, the Column class:
   *
   *   var column1 = Column();
   *   var column2 = Column(
   *     children: const <Widget>[
   *       Text('Abc'),
   *     ],
   *     textDirection: TextDirection.ltr,
   *   );
   *
   * This is specific to Dart, and cannot be done in C++. To create a similar
   * experience, chaining was used:
   *
   *   auto column1 = new Column(parent);
   *   auto column2 = (new Column(parent))
   *   ->children(vector<Widget*>{
   *     new Text("Abc"),
   *   })
   *   ->textDirection(TextDirection::ltr);
   *
   * This is done by declaring the setter functions:
   *
   *   Column* children(vector<Widget> *eChildren);
   *
   * One problem with this, if a class derives from a Column, chaining
   * would give back a Column* after the first setter, so the setters have to be
   * called in a certain order.
   *
   * For arbitrary ordering, there has to be a cast somewhere, preferably not on
   * the user side. For this, a template is used:
   *
   *   template<
   *     class C = Column,
   *     class = typename enable_if_t<is_base_of_v<Column, Derived>>>
   *   C* children(vector<Widget> *eChildren);
   *
   * Now users can chain in arbitrary order, as long as they cast correctly.
   *
   *   auto column = (new Derived(parent))
   *     ->derivedClassSetter1(...)
   *     ->textDirection<Derived>(TextDirection::ltr)
   *     ->derivedClassSetter2(...)
   *
   * This seems acceptable, but the template parameter can be wrong.
   * The mspCheckCast function makes sure that there will be a runtime error if
   * the instantiated object cannot be cast into the class given as the template
   * parameter.
   *
   * Chaining setters should include this check at the top of the function.
   */
  template <class C>
  void mspCheckCast() {
    if (dynamic_cast<C *>(this) == nullptr) {
      throw std::bad_cast();
    }
  }
};
} // namespace msp
#endif // MESSEPLATZ_WIDGET_HPP
