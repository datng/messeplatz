#ifndef MESSEPLATZ_WIDGETSTATEBASE_HPP
#define MESSEPLATZ_WIDGETSTATEBASE_HPP
#include <QObject>

#include "Widget.hpp"

namespace msp {
class WidgetStateBase : public QObject {
  friend class StatefulWidget;

protected:
  virtual void initState() = 0;
  virtual void setState(std::function<void()>) = 0;
  virtual Widget *build() = 0;
};
} // namespace msp
#endif // MESSEPLATZ_WIDGETSTATEBASE_HPP
