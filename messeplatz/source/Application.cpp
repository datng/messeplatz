#include <messeplatz/Application.hpp>

namespace msp {
Application::Application(int &argc, char **argv) : QApplication(argc, argv) {}

auto Application::withHome(Widget *home) -> Application & {
  m_Home = home;
  return *this;
}

Application::~Application() { delete m_Home; }

auto Application::Home() -> Widget * { return m_Home; }

auto runApp(Application *app) -> int {
  assert(app != nullptr);
  app->Home()->show();
  return msp::Application::exec();
}
} // namespace msp
