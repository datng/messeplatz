#include <messeplatz/ExpansionTile.hpp>

#include <QApplication>
#include <QStyle>
#include <QToolButton>

namespace msp {
ExpansionTile::ExpansionTile(Widget *parent) : StatefulWidget(parent) {}

ExpansionTile::~ExpansionTile() = default;

WidgetStateBase *ExpansionTile::createState() {
  return new ExpansionTileState(this);
}

void ExpansionTile::mspReparentChildren() {
  for (auto child : m_Children) {
    child->setParent(this);
  }

  if (m_Leading != nullptr) {
    m_Leading->setParent(this);
  }
  if (m_Title != nullptr) {
    m_Title->setParent(this);
  }
  if (m_Trailing != nullptr) {
    m_Trailing->setParent(this);
  }
}

ExpansionTileState::ExpansionTileState(ExpansionTile *parent)
    : State<ExpansionTile>(parent) {}

Widget *ExpansionTileState::build() {
  auto *content = new Widget();
  widget->setContentsMargins(0, 0, 0, 0);

  auto *vLayout = new QVBoxLayout(content);
  vLayout->setContentsMargins(0, 0, 0, 0);

  auto *header = new Widget(content);
  auto *hLayout = new QHBoxLayout(header);
  hLayout->setContentsMargins(0, 0, 0, 0);

  if (widget->m_Leading != nullptr) {
    vLayout->addWidget(widget->m_Leading);
  }

  if (widget->m_ControlAfinity == ListTileControlAffinity::leading &&
      widget->m_Leading == nullptr) {
    hLayout->addWidget(buildArrowButton(header));

    if (widget->m_Title != nullptr) {
      hLayout->addWidget(widget->m_Title);
    }

    hLayout->addItem(new QSpacerItem(
        0, 0, QSizePolicy::MinimumExpanding, QSizePolicy::Fixed));

    if (widget->m_Trailing != nullptr) {
      hLayout->addWidget(widget->m_Trailing);
    }
  } else if ( //
      widget->m_ControlAfinity == ListTileControlAffinity::trailing &&
      widget->m_Trailing == nullptr) {
    if (widget->m_Leading != nullptr) {
      hLayout->addWidget(widget->m_Leading);
    }

    if (widget->m_Title != nullptr) {
      hLayout->addWidget(widget->m_Title);
    }

    hLayout->addItem(new QSpacerItem(
        0, 0, QSizePolicy::MinimumExpanding, QSizePolicy::Fixed));
    hLayout->addWidget(buildArrowButton(header));
  }

  vLayout->addWidget(header);

  if (widget->m_IsExpanded) {
    for (auto child : widget->m_Children) {
      vLayout->addWidget(child);
    }
  }

  vLayout->addItem(
      new QSpacerItem(0, 0, QSizePolicy::Fixed, QSizePolicy::MinimumExpanding));
  return content;
}

QToolButton *ExpansionTileState::buildArrowButton(Widget *parent) {
  QToolButton *button = new QToolButton(parent);
  button->setFixedSize(20, 20);
  if (widget->m_IsExpanded) {
    button->setIcon(QApplication::style()->standardIcon(QStyle::SP_ArrowDown));
  } else {
    button->setIcon(QApplication::style()->standardIcon(QStyle::SP_ArrowRight));
  }

  connect(button, &QToolButton::clicked, button, [this] {
    setState([this] { widget->m_IsExpanded = !(widget->m_IsExpanded); });
  });
  button->setStyleSheet("background-color:#ffffff;");
  return button;
}
} // namespace msp
