#include <messeplatz/StatefulWidget.hpp>

namespace msp {
StatefulWidget::StatefulWidget(Widget *parent) : Widget(parent) {
  QMetaObject::invokeMethod( //
      this,
      [this] {
        m_State.reset(createState());
        m_State->initState();
      },
      Qt::ConnectionType::QueuedConnection);
}

StatefulWidget::~StatefulWidget() = default;
} // namespace msp
