#include <messeplatz/StatelessWidget.hpp>

#include <QVBoxLayout>

namespace msp {
StatelessWidget::StatelessWidget(Widget *parent) : Widget(parent) {
  QMetaObject::invokeMethod( //
      this,
      [this] {
        delete layout();
        auto layout = new QVBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);
        auto content = build();
        content->setParent(this);
        layout->addWidget(content);
        connect(layout, &QObject::destroyed, content, &QObject::deleteLater);
        setLayout(layout);
      },
      Qt::ConnectionType::QueuedConnection);
}

StatelessWidget::~StatelessWidget() = default;
} // namespace msp
