#include <messeplatz/Text.hpp>

#include <QLabel>

namespace msp {
Text::Text(std::string eData, Widget *parent) : StatelessWidget(parent) {
  setMinimumHeight(20);
  m_Data = eData;
}
Text::~Text() {}

Widget *Text::build() {
  auto content = new Widget;
  auto label = new QLabel(m_Data.c_str(), content);
  return content;
}
} // namespace msp
