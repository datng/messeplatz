#include <messeplatz/Widget.hpp>

namespace msp {
Widget::Widget(Widget *parent) : QWidget(parent) {}
Widget::~Widget() = default;
} // namespace msp
